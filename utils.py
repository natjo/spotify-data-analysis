def sort_dict_by_value(d, reverse):
    """
    Returns a sorted list
    """
    return sorted(d.items(), key=lambda item: item[1], reverse=reverse)

def sort_dict_by_key(d, reverse):
    """
    Returns a sorted list
    """
    return sorted(d.items(), key=lambda item: item[0], reverse=reverse)
