from spotify_data import SpotifyData
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pretty_errors


def main():
    data = SpotifyData("MyData")

    t_all = [x[0] for x in data.all_dates_counts]
    y_all = [x[1] for x in data.all_dates_counts]

    artists = ["Heaven Shall Burn", "Alestorm", "Kreator", "Rigoros"]
    for artist in artists:
        data_rdcp = data.get_date_counts_of_artist(artist, t_all)
        t = [x[0] for x in data_rdcp]
        y = [x[1] for x in data_rdcp]

        plt.plot(t, y, label=artist)

    frame1 = plt.gca()
    frame1.axes.get_xaxis().set_visible(False)


def make_plot_pretty():
    Blue = "#2CBDFE"
    Green = "#47DBCD"
    Pink = "#F3A0F2"
    Purple = "#9D2EC5"
    Violet = "#661D98"
    Amber = "#F5B14C"

    color_list = [Blue, Pink, Green, Amber, Purple, Violet]
    CB_color_cycle_colorblind = [
        "#377eb8",
        "#ff7f00",
        "#4daf4a",
        "#f781bf",
        "#a65628",
        "#984ea3",
        "#999999",
        "#e41a1c",
        "#dede00",
    ]

    plt.rcParams["axes.prop_cycle"] = plt.cycler(color=CB_color_cycle_colorblind)
    plt.clf()
    ax = plt.subplot(111)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    plt.grid(True)


if __name__ == "__main__":
    make_plot_pretty()
    main()
    plt.legend()
    plt.show()
