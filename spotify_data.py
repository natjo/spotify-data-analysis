import json
import os
from collections import Counter
from pprint import pprint
from utils import sort_dict_by_value, sort_dict_by_key

# For debuging
# FILE_NAMES = ['StreamingHistory_small0.json', 'StreamingHistory_small1.json']
FILE_NAMES = [
    "StreamingHistory0.json",
    "StreamingHistory1.json",
    "StreamingHistory2.json",
    "StreamingHistory3.json",
]

PLAYLIST_FILE_NAME = "Playlist1.json"


class SpotifyData:
    def __init__(self, data_dir):
        self._data_dir = data_dir

        # Defined to be cached later on
        self._songs_raw = None
        self._unique_artists = None
        self._artist_plays_counts = None
        self._songs_played_counts = None
        self._all_playlist_names = None
        self._playlists_raw = None
        self._all_dates_counts = None

    ### Artist related things
    @property
    def unique_artists(self):
        """
        List of all artists listened to
        """
        if self._unique_artists:
            return self._unique_artists
        self._unique_artists = [x[0] for x in self.artist_plays_counts]
        return self._unique_artists

    @property
    def artist_plays_counts(self):
        """
        Returns a dictionary {artist_name: count_total_song_plays}
        """
        if self._artist_plays_counts:
            return self._artist_plays_counts
        all_artists = [x["artistName"] for x in self.songs_raw]
        unsorted_artist_plays_counts = dict(Counter(all_artists))
        self._artist_plays_counts = sort_dict_by_value(
            unsorted_artist_plays_counts, reverse=True
        )
        return self._artist_plays_counts

    # Song related things

    @property
    def songs_played_counts(self):
        """
        Returns a dictionary {song_name: count_total_times_played}
        """
        if self._songs_played_counts:
            return self._songs_played_counts
        all_songs = [x["trackName"] for x in self.songs_raw]
        unsorted_songs_played_counts = dict(Counter(all_songs))
        self._songs_played_counts = sort_dict_by_value(
            unsorted_songs_played_counts, reverse=True
        )
        return self._songs_played_counts

    def get_songs_played_counts_filtered(self, filter_playlist_names):
        filter_songs = []
        for filter_playlist in filter_playlist_names:
            filter_songs.extend(self.get_songs_from_playlist(filter_playlist))
        return list(
            filter(lambda x: x[0] not in filter_songs, self.songs_played_counts)
        )

    ### Playlist related things

    @property
    def all_playlist_names(self):
        """
        List of playlist names
        """
        if self._all_playlist_names:
            return self._all_playlist_names
        playlists_raw = self._load_playlists_raw()
        self._all_playlist_names = [x["name"] for x in playlists_raw]
        return self._all_playlist_names

    def get_songs_from_playlist(self, playlist_name):
        playlist_songs_raw = self._get_songs_from_playlist_raw(playlist_name)
        return [x["track"]["trackName"] for x in playlist_songs_raw]

    def _get_songs_from_playlist_raw(self, playlist_name):
        playlists_raw = self._load_playlists_raw()
        playlist_raw = [x for x in playlists_raw if x["name"] == playlist_name][0]
        playlist_songs_raw = playlist_raw["items"]
        # Some elements have no track information, remove those
        playlist_songs_raw = list(
            filter(lambda x: x["track"] is not None, playlist_songs_raw)
        )
        return playlist_songs_raw

    def _load_playlists_raw(self):
        if self._playlists_raw:
            return self._playlists_raw
        path = os.path.join(self._data_dir, PLAYLIST_FILE_NAME)
        f = open(path)
        self._playlists_raw = json.load(f)["playlists"]
        return self._playlists_raw

    @property
    def songs_raw(
        self,
    ):
        if self._songs_raw:
            return self._songs_raw
        self._songs_raw = []
        for file_name in FILE_NAMES:
            path = os.path.join(self._data_dir, file_name)
            f = open(path)
            self._songs_raw.extend(json.load(f))
        return self._songs_raw

    ### Other

    @property
    def all_dates_counts(self):
        """
        List of tuple (date, songs_played_count)
        """
        if self._all_dates_counts:
            return self._all_dates_counts
        self._all_dates_counts = self._get_dates_counts(self.songs_raw)
        return self._all_dates_counts

    def get_date_counts_of_artist(self, artist, dates=None):
        """
        Returns list of tuple (date, songs_played_count)
        If dates is provided as a list of strings of dates YYYY-MM-DD
        The returned list will be filled up with 0 counts on all dates specified the artist did not play
        """
        songs_raw_filtered = filter(lambda x: x["artistName"] == artist, self.songs_raw)
        dates_counts = self._get_dates_counts(songs_raw_filtered)
        if dates:
            dates_counts_dict = {x[0]: x[1] for x in dates_counts}
            dates_counts = [
                (d, dates_counts_dict[d]) if d in dates_counts_dict.keys() else (d, 0)
                for d in dates
            ]
        return dates_counts

    def _get_dates_counts(self, songs_raw):
        all_dates = [self._get_ymd_from_date(x["endTime"]) for x in songs_raw]
        unsorted_all_dates_counts = dict(Counter(all_dates))
        return sort_dict_by_key(unsorted_all_dates_counts, reverse=False)

    # Original date looks like this: 2020-11-09 15:52
    # We are only in YYYY-MM-DD interested
    def _get_ymd_from_date(self, date):
        return date[:-6]
