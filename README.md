# Spotify Data Analysis
You can get all your personal data from Spotify. This project lays some basic infrastructure to analyse it.

## How to use
Extract your data into `MyData`. That's where the code expects to find all files.

Use `main.py` to print some statistics, `plot.py` for some graphs like this one:

![](/example_graph.png)