from spotify_data import SpotifyData
from pprint import pprint
from tabulate import tabulate
import pretty_errors


def main():
    data = SpotifyData("MyData")

    # print(data.unique_artists)
    # print(data.playlist_names)
    # print(data.get_songs_from_playlist("Sing along"))
    # return

    print("Most listened Artists")
    print_table(data.artist_plays_counts, ["Artist", "Songs Played"])
    print()

    print("Most listened Songs")
    print_table(data.songs_played_counts, ["Song", "Times Played"])
    print()

    filter_playlists = [
        "Bayrische Schmankerl",
        "Harry Potter & Co",
        "Jonas' Delight",
        "Christmas - All The Holiday Gems",
    ]
    print(f"Most listened Songs not in \"{', '.join(filter_playlists)}\"")
    print_table(
        data.get_songs_played_counts_filtered(filter_playlists),
        ["Song", "Times Played"],
    )
    print()

    filter_playlists = data.all_playlist_names
    print(f"Most listened Songs not in any playlist")
    print_table(
        data.get_songs_played_counts_filtered(filter_playlists),
        ["Song", "Times Played"],
    )

    print(f"Total unique artists listened to: {len(data.unique_artists)}")
    print(f"Total unique songs listened to: {len(data.songs_played_counts)}")


def print_table(data, headers, displayed_elements=20):
    print(tabulate(data[:displayed_elements], headers=headers))


if __name__ == "__main__":
    main()
